(function() {
  "use strict";

  if (typeof TTT === 'undefined') {
    window.TTT = {};
  }

  var TicTacToeNode = TTT.TicTacToeNode = function(board, nextMark, prevMovePos) {
    this.board = board;
    this.nextMark = nextMark;
    this.prevMovePos = prevMovePos;
  };
  TicTacToeNode.prototype.losingNode = function(mark) {
    if (this.board.over()) {
      return this.board.won() && this.board.winner() !== mark;
    }

    if (this.nextMark === mark) {
      return this.children().every(function(node) {
        return node.losingNode(mark);
      });
    } else {
      return this.children().some(function(node) {
        return node.losingNode(mark);
      });
    }
  };

  TicTacToeNode.prototype.winningNode = function(mark) {
    if (this.board.over()) {
      return this.board.won() && this.board.winner() === mark;
    }

    if (this.nextMark === mark) {
      return this.children().some(function(node) {
        return node.winningNode(mark);
      });
    } else {
      return this.children().every(function(node) {
        return node.winningNode(mark);
      });
    }
  };

  TicTacToeNode.prototype.children = function() {
    var childrenArr = [];
    for (var rowIdx = 0; rowIdx < 3; rowIdx++) {
      for(var colIdx = 0; colIdx < 3; colIdx++) {
        var pos = [rowIdx, colIdx];
        if (!this.board.empty(pos)) {
          continue;
        }
        var newBoard =  this.board.dup();
        newBoard.getTile(pos).content = this.nextMark;
        var nextMark = (this.nextMark === "X" ? "O" : "X");
        childrenArr.push(new TicTacToeNode(newBoard, nextMark, pos));
      }
    }
    return childrenArr;
  };
})();
